# main.tf

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
  }
  required_version = ">= 0.13"
}

################################################################################
# Google Compute Engine (GCE) Network Configuration                            #
################################################################################

# GCP VPC network for environment resources
resource "google_compute_network" "vpc_network" {
  auto_create_subnetworks = "false"
  description             = "VPC for ${var.env_name}"
  name                    = "${var.env_prefix}vpc"
  routing_mode            = "REGIONAL"
}

# GCP Cloud Router for VPC network
resource "google_compute_router" "router" {
  name    = "${var.env_prefix}vpc-router"
  network = google_compute_network.vpc_network.name
  region  = var.gcp_region
}

# GCP NAT Gateway for VPC network
resource "google_compute_router_nat" "nat_gateway" {
  name                               = "${var.env_prefix}vpc-nat-gateway"
  nat_ip_allocate_option             = "AUTO_ONLY"
  router                             = google_compute_router.router.name
  region                             = var.gcp_region
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  # You can enable logging once monitoring infrastructure is ready
  log_config {
    enable = "false"
    filter = "ALL"
  }
}

# GCP Subnet for instances and services
# The cidrsubnet function converts the region CIDR of "10.128.0.0/12" into
# a range of "10.128.20.0/24" which is a convention that is used by the
# GitLab Demo Systems. You can customize this to any CIDR range if needed.
resource "google_compute_subnetwork" "region_subnet" {
  name          = "${var.env_prefix}${var.gcp_region}-subnet"
  ip_cidr_range = cidrsubnet(var.gcp_region_cidr, 12, 20)
  region        = var.gcp_region
  network       = google_compute_network.vpc_network.self_link
  depends_on = [
    google_compute_network.vpc_network
  ]
}

# GCP DNS Zone for Environment (can be root or subdomain)
resource "google_dns_managed_zone" "dns_zone" {
  description = "Managed DNS zone for ${var.env_name}"
  dns_name    = var.gcp_dns_zone_fqdn
  name        = "${var.env_prefix}dns-zone"
}

################################################################################
# GitLab Omnibus Google Compute Engine (GCE) Instance Configuration            #
################################################################################

# GitLab Omnibus instance
module "gitlab_omnibus_instance" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gce/gcp-compute-instance-tf-module.git"
  # source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gce/gcp-compute-instance-tf-module.git?ref=0.4.0"

  # Required variables
  gcp_machine_type     = var.env_sandbox_mode ? "e2-standard-4" : var.gitlab_omnibus.gcp_machine_type
  gcp_project          = var.gcp_project
  gcp_region           = var.gcp_region
  gcp_region_zone      = var.gcp_region_zone
  gcp_subnetwork       = google_compute_subnetwork.region_subnet.self_link
  instance_description = "GitLab Omnibus instance for ${var.env_name}"
  instance_name        = "${var.env_prefix}gitlab-omnibus-instance"

  # Optional variables with default values
  disk_boot_size          = var.env_sandbox_mode ? "30" : var.gitlab_omnibus.disk_boot_size
  disk_storage_enabled    = "true"
  disk_storage_size       = var.env_sandbox_mode ? "100" : var.gitlab_omnibus.disk_storage_size
  dns_create_record       = "true"
  dns_ttl                 = "300"
  gcp_deletion_protection = var.env_sandbox_mode ? "false" : var.gitlab_omnibus.gcp_deletion_protection
  gcp_dns_zone_name       = google_dns_managed_zone.dns_zone.name
  gcp_image               = var.env_sandbox_mode ? "ubuntu-1804-lts" : var.gitlab_omnibus.gcp_image
  gcp_network_tags        = ["${var.env_prefix}gitlab-omnibus-firewall-rule"]

  # Labels for metadata and cost analytics
  labels = merge(var.labels, {
    gl_resource_type  = "compute-instance",
    gl_resource_group = "gitlab-omnibus-instance",
    gl_resource_name  = "${var.env_prefix}gitlab-omnibus-instance"
  })

  depends_on = [
    google_compute_subnetwork.region_subnet,
    google_dns_managed_zone.dns_zone,
  ]

}

# Subdomain @ record for the Omnibus instance
# This will allow your Omnibus instance be accessible without the hostname FQDN.
# env-name.mydomain.tld -and/or- gitlab-omnibus-instance.env-name.mydomain.td
resource "google_dns_record_set" "gitlab_omnibus_subdomain_dns_record" {
  managed_zone = google_dns_managed_zone.dns_zone.name
  name         = google_dns_managed_zone.dns_zone.dns_name
  type         = "A"
  rrdatas      = [module.gitlab_omnibus_instance.network.external_ip]
  ttl          = 300
}

# Firewall rule for GitLab Omnibus instance
# https://docs.gitlab.com/omnibus/package-information/defaults.html
resource "google_compute_firewall" "gitlab_omnibus_firewall_rule" {
  name    = "${var.env_prefix}gitlab-omnibus-firewall-rule"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports = [
      "22",
      "80",
      "143",
      "443",
      "465",
      "514",
      "5000",
      "5050",
      "5431",
      "5432",
      "6379",
      "6432",
      "8008",
      "8060",
      "8065",
      "8075",
      "8080",
      "8082",
      "8083",
      "8088",
      "8150",
      "8181",
      "8280",
      "8300",
      "8443",
      "8500",
      "9090",
      "9100",
      "9121",
      "9168",
      "9187",
      "9188",
      "9200",
      "26379",
    ]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["${var.env_prefix}gitlab-omnibus-firewall-rule"]
}

################################################################################
# GitLab Runner Manager Google Compute Engine (GCE) Instance Configuration     #
################################################################################

# GitLab Runner Manager
module "gitlab_runner_manager_instance" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gce/gcp-compute-instance-tf-module.git"
  # source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gce/gcp-compute-instance-tf-module.git?ref=0.4.0"

  # Required variables
  gcp_machine_type     = var.env_sandbox_mode ? "e2-small" : var.gitlab_runner_manager.gcp_machine_type
  gcp_project          = var.gcp_project
  gcp_region           = var.gcp_region
  gcp_region_zone      = var.gcp_region_zone
  gcp_subnetwork       = google_compute_subnetwork.region_subnet.self_link
  instance_description = "GitLab Runner Manager instance for ${var.env_name}"
  instance_name        = "${var.env_prefix}gitlab-runner-manager-instance"

  # Optional variables with default values
  disk_boot_size          = var.env_sandbox_mode ? "20" : var.gitlab_runner_manager.disk_boot_size
  disk_storage_enabled    = "false"
  dns_create_record       = "true"
  dns_ttl                 = "300"
  gcp_deletion_protection = var.env_sandbox_mode ? "false" : var.gitlab_runner_manager.gcp_deletion_protection
  gcp_dns_zone_name       = google_dns_managed_zone.dns_zone.name
  gcp_image               = var.env_sandbox_mode ? "ubuntu-1804-lts" : var.gitlab_runner_manager.gcp_image
  gcp_network_tags        = ["${var.env_prefix}gitlab-runner-manager-firewall-rule"]

  # Labels for metadata and cost analytics
  labels = merge(var.labels, {
    gl_resource_type  = "compute-instance",
    gl_resource_group = "gitlab-runner-manager-instance",
    gl_resource_name  = "${var.env_prefix}gitlab-runner-manager-instance"
  })

  depends_on = [
    google_compute_subnetwork.region_subnet,
    google_dns_managed_zone.dns_zone,
  ]

}

# Firewall rule for GitLab Omnibus instance
# https://docs.gitlab.com/omnibus/package-information/defaults.html
resource "google_compute_firewall" "gitlab_runner_manager_firewall_rule" {
  name    = "${var.env_prefix}gitlab-runner-manager-firewall-rule"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports = [
      "22",
      "443",
      "2376",
      "2377",

    ]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["${var.env_prefix}gitlab-runner-manager-firewall-rule"]
}

################################################################################
# Google Kubernetes Engine (GKE) Cluster Configuration                         #
################################################################################

# Kubernetes cluster for GitLab CI jobs
module "gitlab_ci_cluster" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module.git"

  # Required variables
  cluster_name     = "${var.env_prefix}gitlab-ci-cluster"
  description      = "GitLab CI instance-level Kubernetes cluster"
  gcp_network      = google_compute_network.vpc_network.self_link
  gcp_project      = var.gcp_project
  gcp_region       = var.gcp_region
  gcp_region_zone  = var.gcp_region_zone
  gke_cluster_cidr = var.env_sandbox_mode ? cidrsubnet(cidrsubnet(var.gcp_region_cidr, 4, 12), 6, 0) : cidrsubnet(var.gcp_region_cidr, 4, 3)

  # The cidrsubnet function converts a region CIDR of "10.128.0.0/12" into a
  # range of "10.131.0.0/16" which is a convention that is used by the GitLab
  # Demo Systems for a large-sized cluster. You can customize this with a
  # different CIDR range supported by the Terraform module for a small, medium,
  # or large sized cluster. See Terraform module SIZING.md docs for details.
  # Small cluster => cidrsubnet(cidrsubnet(var.gcp_region_cidr, 4, 12), 6, 0) => 10.140.0.0/22
  # Medium cluster => cidrsubnet(cidrsubnet(var.gcp_region_cidr, 4, 2), 4, 0) => 10.130.0.0/20
  # Large cluster => cidrsubnet(var.gcp_region_cidr, 4, 3) => 10.131.0.0/16

  # Optional variables with default values
  gcp_machine_type           = var.env_sandbox_mode ? "e2-standard-2" : var.gitlab_ci_cluster.gcp_machine_type
  gcp_preemptible_nodes      = var.env_sandbox_mode ? "true" : var.gitlab_ci_cluster.gcp_preemptible_nodes
  gke_autoscaling_profile    = var.env_sandbox_mode ? "BALANCED" : var.gitlab_ci_cluster.gke_autoscaling_profile
  gke_cluster_type           = var.env_sandbox_mode ? "zonal" : var.gitlab_ci_cluster.gke_cluster_type
  gke_node_count_max         = var.env_sandbox_mode ? 8 : var.gitlab_ci_cluster.gke_cluster_type == "zonal" ? 512 : 176
  gke_node_count_min         = 1
  gke_node_pods_count_max    = 32
  gke_maintenance_start_time = var.env_sandbox_mode ? "03:00" : var.gitlab_ci_cluster.gke_maintenance_start_time
  gke_release_channel        = var.env_sandbox_mode ? "REGULAR" : var.gitlab_ci_cluster.gke_release_channel
  gke_version_prefix         = var.env_sandbox_mode ? "1.18." : var.gitlab_ci_cluster.gke_version_prefix

  # Labels for metadata and cost analytics
  labels = merge(var.labels, {
    gl_resource_type  = "compute-instance",
    gl_resource_group = "gitlab-ci-cluster",
    gl_resource_name  = "${var.env_prefix}gitlab-ci-cluster-node"
  })

  depends_on = [
    google_compute_network.vpc_network
  ]
}

# Create a DNS record for cluster endpoint ingress resources
resource "google_dns_record_set" "gitlab_ci_cluster_dns_record_subdomain" {
  count = var.gitlab_ci_cluster_ingress.enabled ? 1 : 0

  managed_zone = google_dns_managed_zone.dns_zone.name
  name         = "ci.${google_dns_managed_zone.dns_zone.dns_name}"
  rrdatas      = [var.gitlab_ci_cluster_ingress.external_ip]
  ttl          = "300"
  type         = "A"

  depends_on = [
    google_dns_managed_zone.dns_zone
  ]
}

# Create a wildcard DNS subdomain record for cluster endpoint ingress resources
resource "google_dns_record_set" "gitlab_ci_cluster_dns_record_wildcard" {
  count = var.gitlab_ci_cluster_ingress.enabled ? 1 : 0

  managed_zone = google_dns_managed_zone.dns_zone.name
  name         = "*.ci.${google_dns_managed_zone.dns_zone.dns_name}"
  rrdatas      = [var.gitlab_ci_cluster_ingress.external_ip]
  ttl          = "300"
  type         = "A"

  depends_on = [
    google_dns_managed_zone.dns_zone
  ]
}

# Create DNS record for CertBot TXT verification
resource "google_dns_record_set" "gitlab_ci_cluster_dns_record_txt" {
  count = var.gitlab_ci_cluster_letsencrypt_txt.enabled ? 1 : 0

  managed_zone = google_dns_managed_zone.dns_zone.name
  name         = "_acme-challenge.ci.${google_dns_managed_zone.dns_zone.dns_name}"
  rrdatas      = [var.gitlab_ci_cluster_letsencrypt_txt.value]
  ttl          = "300"
  type         = "TXT"

  depends_on = [
    google_dns_managed_zone.dns_zone
  ]
}
