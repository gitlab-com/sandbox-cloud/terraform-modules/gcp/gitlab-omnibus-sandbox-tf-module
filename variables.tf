# variables.tf

variable "env_name" {
  type        = string
  description = "A friendly name to describe the environment. (Example: Dade Murphy Omnibus Sandbox Environment)"
}

variable "env_prefix" {
  type        = string
  description = "A alphadash short abbreviation of the environment name with a trailing dash. (Example: dmurphy-)"
}

variable "env_sandbox_mode" {
  type        = bool
  description = "If sandbox mode is enabled, infrastructure resources are downsized to accomodate less than 10 users and are cost efficient with machine types and high-availability requirements. See the gitlab_omnibus and gitlab_ci_cluster variable defaults below for sandbox mode values."
  default     = true
}

variable "gcp_dns_zone_fqdn" {
  type        = string
  description = "The FQDN of the GCP Cloud DNS zone that will be created for this environment, with a trailing period. (Example: gitlab.example.tld.)"
}

variable "gcp_project" {
  type        = string
  description = "The GCP project ID that the resources will be deployed in. (Example: my-project-name)"
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in. (Ex. us-east1)"
}

variable "gcp_region_cidr" {
  type        = string
  description = "A /12 CIDR range for the GCP region that will be used for dynamically creating subnets. You can leave this at the default value for most use cases. (Ex. 10.128.0.0/12, 10.144.0.0/12, 10.160.0.0/12)"
  default     = "10.128.0.0/12"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone that the resources will be deployed in. This must match the region. (Example: us-east1-c)"
}

variable "gitlab_omnibus" {
  type = object({
    disk_boot_size          = number
    disk_storage_size       = number
    gcp_deletion_protection = bool
    gcp_image               = string
    gcp_machine_type        = string
  })
  description = "The sizing values for custom-sized GitLab Omnibus instance. These values are ignored if env_sandbox_mode is true."
  default = {
    "disk_boot_size" : "30",
    "disk_storage_size" : "100",
    "gcp_deletion_protection" : "false",
    "gcp_image" : "ubuntu-1804-lts",
    "gcp_machine_type" : "e2-standard-4"
  }
}

variable "gitlab_runner_manager" {
  type = object({
    disk_boot_size          = number
    gcp_deletion_protection = bool
    gcp_image               = string
    gcp_machine_type        = string
  })
  description = "The sizing values for custom-sized GitLab Runner Manager instance. These values are ignored if env_sandbox_mode is true."
  default = {
    "disk_boot_size" : "20",
    "gcp_deletion_protection" : "false",
    "gcp_image" : "ubuntu-1804-lts",
    "gcp_machine_type" : "e2-small"
  }
}

variable "gitlab_ci_cluster" {
  type = object({
    gcp_machine_type           = string
    gcp_preemptible_nodes      = bool
    gke_autoscaling_profile    = string
    gke_cluster_type           = string
    gke_maintenance_start_time = string
    gke_release_channel        = string
    gke_version_prefix         = string
  })
  description = "The sizing values for custom-sized GitLab CI Kubernetes cluster. These values are ignored if env_sandbox_mode is true."
  default = {
    "gcp_machine_type" : "e2-standard-2",
    "gcp_preemptible_nodes" : "true",
    "gke_autoscaling_profile" : "BALANCED",
    "gke_cluster_type" : "zonal",
    "gke_maintenance_start_time" : "03:00",
    "gke_release_channel" : "REGULAR",
    "gke_version_prefix" : "1.18."
  }
}

variable "gitlab_ci_cluster_ingress" {
  type = object({
    enabled     = bool
    external_ip = string
  })
  description = "The external IP address that is associated with the ingress service after the helm chart has been deployed."
  default = {
    "enabled" : "false",
    "external_ip" : "1.2.3.4"
  }
}

variable "gitlab_ci_cluster_letsencrypt_txt" {
  type = object({
    enabled = bool
    value   = string
  })
  description = "The certbot verification process will create a TXT string that needs to be added to the DNS zone to complete LetsEncrypt SSL verification."
  default = {
    "enabled" : "false",
    "value" : "placeholder"
  }
}

variable "labels" {
  type        = map(any)
  description = "Labels to place on the GCP infrastructure resources. Label keys should use underscores, values should use alphadash format. No spaces are allowed."
}
