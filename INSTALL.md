# GCP Terraform Module for Compute Instance with DNS Record

## Table of Contents

* [Prerequisites](#prerequisites)
* [Step-by-Step Instructions](#step-by-step-instructions)
* [Variables, Outputs, and Additional Customization](#variables-outputs-and-additional-customization)

## Prerequisites

1. Create a Git repository for your Terraform project, or use an existing repository with your Terraform configuration. **For security reasons, it is imperative that your [GitLab](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility) or [GitHub](https://docs.github.com/en/github/administering-a-repository/setting-repository-visibility) project is a `private` project (not `public` or `internal`) to avoid leaking your infrastructure credentials.**

1. Create a <a target="_blank" href="https://cloud.google.com/resource-manager/docs/creating-managing-projects">GCP project</a> or use an existing GCP project. You will need to decide which <a target="_blank" href="https://cloud.google.com/compute/docs/regions-zones">region and zone</a> you will use. The examples in this module use `us-east1` and `us-east1-c`.

1. In order to make requests against the GCP API, you need to authenticate to prove that it's you making the request. The preferred method of provisioning resources with Terraform is to use a GCP service account, which is a "robot account" that can be granted a limited set of IAM permissions. Use the [Terraform provider instructions](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#adding-credentials) for creating a service account using the Google Cloud Console or `gcloud` CLI tool.

    > You can use the `GOOGLE_APPLICATION_CREDENTIALS` environment variable, however if you're working with multiple GCP projects you may find it easier to work use a key file in each of your repositories. In our examples, we use the `keys/gcp-service-account.json` file however you can name it whatever you would like (ex. my-project-name-a1b2c3d4.json).

    > It is important that you do not commit the service account `.json` file to your Git repository since this compromises your credentials. You should add the `/keys` directory to your `.gitignore` file. See the `.gitignore.example` file in this module for example configuration.

1. A VPC network and subnetwork will be created automatically by this module. This will not conflict with existing VPC networks or subnets, however you will not be able to communicate with other resources in your GCP project that are in a different VPC. Ideally this module should only be used for isolated sandbox environments. You can deploy additional resources into the project and subnet after the resources in this module have been created.

1. This module creates a managed DNS zone for an FQDN. You can use the subdomain of an existing domain name (ex. `gitlab.example.tld`) or you can purchase or use an existing normal domain name (ex. `example.tld`). The module outputs will include the DNS name servers that you will need to configure. Each of the resources will be created as an A record in the managed zone, and a `@` record to be able to access the Omnibus instance using the FQDN that you specify.

## GitLab CI Variables

If your Terraform project uses GitLab CI for running `terraform` commands, you optionally can use GitLab CI variables instead of the `terraform.tfvars.json` file.

The table below shows the example usage for the `examples/gitlab-sandbox-cloud` variables. You can define additional variables with the `TF_VAR_` case-sensitive prefix on any variable in your `variables.tf` file.

| CI Variable | Example Value | Instructions |
|-------------|---------------|--------------|
| `TF_VAR_owner_full_name`      | `Dade Murphy` | The first and last name of the owner (without special punctuation). |
| `TF_VAR_owner_email_handle`   | `dmurphy` | The first initial and last name email handle of the owner. |
| `TF_VAR_gl_dept`              | `sales-cs` | The GitLab department slug that the owner belongs to. See [infrastructure standards handbook](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/#gitlab-department-gl_dept) for a list of expected values. |
| `TF_VAR_gl_dept_group`        | `sales-cs-sa-us-west` | The GitLab department group (team) slug that the owner belongs to. See [infrastructure standards handbook](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/#gitlab-department-group-gl_dept_group) for a list of expected values. |
| `TF_VAR_gcp_region`           | `us-central1` | The GCP region that the resources will be deployed in. |
| `TF_VAR_gcp_region_zone`      | `us-central1-c` | The GCP region availability zone that the resources will be deployed in. This must match the region. |

## Step-by-Step Instructions

1. Review the contents of `main.tf` to understand what is getting created. You can also review the child modules using the Git URLs found in the `source` for each module. You can see how this module is used in the `examples/*` directories. If this is your first time using this module, you should use `examples/experiment` to perform a proof-of-concept deployment.

1. Open your text editor and navigate to your new or existing Terraform project (also referred to as your "environment configuration").

1. Copy and paste the contents of `examples/{{example-name}}/main.tf` to the `main.tf` file in your Terraform project. You can add to the bottom of the file or create the file if it does not exist.

    > If the `provider "google" {}` and `provider "google-beta" {}` blocks already exist in your `main.tf` file, you do not need to add them twice.

1. Copy and paste the contents of `examples/{{example-name}}/outputs.tf` to the `outputs.tf` file in your Terraform project. You can add to the bottom of the file or create the file if it does not exist.

1. Copy and paste the contents of `examples/{{example-name}}/variables.tf` to the `variables.tf` file in your Terraform project. You can add to the bottom of the file or create the file if it does not exist.

1. Copy and paste the `examples/{{example-name}}/.gitignore.example` file to your Terraform project. If you do not have a `.gitignore` file, you can simply remove the `.example` extension. If you have a `.gitignore` file, take a few moments to add the lines from `.gitignore.example` to your existing file.

    > It is your discretion whether your `.tfvars` files are committed to your source code, however we have included them in the `.gitignore` as a security precaution.

1. Copy and paste the contents of `examples/{{example-name}}/terraform.tfvars.json` to the `terraform.tfvars.json` file in your Terraform project.

    > If you have an existing `terraform.tfvars` file or another `.tfvars` file, you can create the key/value pairs in your existing file using the appropriate syntax.

1. Update the values in `terraform.tfvars.json` to match your environment configuration. You can see a description of each of the variables in the `variables.tf` file.

    > The GCP project can be either the slug of the project or the 12-digit project ID. See the GCP documentation for [identifying projects](https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects) to learn more.

    ```
    {
        "env_name": "Dade Murphy Omnibus Sandbox Environment",
    	"env_prefix": "dmurphy-",
        "env_sandbox_mode": "true",
    	"gcp_dns_zone_fqdn": "omnibus.mydomain.tld.",
    	"gcp_project": "my-project-name-a1b2c3d4",
    	"gcp_region": "us-central1",
    	"gcp_region_zone": "us-central1-c",
    }
    ```

    > If `env_sandbox_mode` variable is set to `true` (default), all configuration settings are pre-configured with opinionated defaults. If the `env_sandbox_mode` variable is set to `false`, then you can customize additional variables for the `gitlab_omnibus` and `gitlab_ci_cluster` child modules.

    ```
    {
    	"env_name": "Example Environment",
    	"env_prefix": "example-",
    	"env_sandbox_mode" : "false",
    	"gcp_dns_zone_fqdn": "omnibus.mydomain.tld.",
    	"gcp_project": "my-gcp-project-a1b2c3d4",
    	"gcp_region": "us-central1",
    	"gcp_region_cidr": "10.128.0.0/12",
    	"gcp_region_zone": "us-central1-c",
    	"gitlab_omnibus" : {
    		"disk_boot_size" : "30",
    		"disk_storage_size" : "100",
    		"gcp_deletion_protection" : "false",
    		"gcp_image" : "ubuntu-1804-lts",
    		"gcp_machine_type" : "e2-standard-4"
    	},
    	"gitlab_ci_cluster" : {
    		"gcp_machine_type" : "e2-standard-2",
    		"gcp_preemptible_nodes" : "true",
    		"gke_autoscaling_profile" : "BALANCED",
    		"gke_cluster_type" : "zonal",
    		"gke_maintenance_start_time" : "03:00",
    		"gke_release_channel" : "REGULAR",
    		"gke_version_prefix" : "1.18."
    	},
    	"labels": {
    		"gl_realm": "x",
    		"gl_env_type": "x",
    		"gl_env_name": "x",
    		"gl_env_id": "x",
    		"gl_owner_email_handle": "x",
    		"gl_dept": "x",
    		"gl_dept_group": "x"
    	}
    }
    ```

1. Create a new file named `gcp-service-account.json` in the `keys/` directory with your service account credentials JSON keyfile.

    ```
    {
      "type": "service_account",
      "project_id": "demosys-saas",
      "private_key_id": "NULL",
      "private_key": "-----BEGIN PRIVATE KEY-----\nNULL\n-----END PRIVATE KEY-----\n",
      "client_email": "NULL-compute@developer.gserviceaccount.com",
      "client_id": "NULL",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/NULL-compute%40developer.gserviceaccount.com"
    }
    ```

    > If you are using `gcloud` or `GOOGLE_APPLICATION_CREDENTIALS` environment variable, comment out or remove `credentials = file("./keys/gcp-service-account.json")` in the `provider "google" {}` and `provider "google-beta" {}` blocks in `main.tf`.

    ```
    # [terraform-project]/main.tf

    # Define the Google Cloud Provider
    provider "google" {
      # credentials = file("./keys/gcp-service-account.json")
      project     = var.gcp_project
    }

    # Define the Google Cloud Provider with beta features
    provider "google-beta" {
      # credentials = file("./keys/gcp-service-account.json")
      project     = var.gcp_project
    }
    ```

1. Open your Terminal and navigate to the directory where your Terraform environment configuration resides.

    ```
    cd ~/Sites/terraform-project
    ```

1. Run `terraform init` to initialize your Terraform configuration and create a Terraform state file.

    ```
    terraform init
    ```

    > If there are any problems with authenticating with the Google API, you will see the errors during this step. These are generic Terraform errors and are not specific to this module usually. You should be able to perform a Google search to get help with the error message that you're seeing.

1. Run `terraform plan` to see how your Terraform configuration will make changes to your infrastructure. If this is your first time using this Terraform module, you should take a few minutes to review all of the infrastructure resources that will be created and their expected values.

    ```
    terraform plan
    ```

1. Run `terraform apply` to deploy the resources that were shown in the plan output.

    ```
    terraform apply
    ```

1. If this was an experiment and you're ready to clean up your work, run `terraform destroy` to remove the entire environment. If you only want to rebuild a component of the environment, you can use targeting. **If you do not specify a target, all of your Terraform-managed infrastructure will be permanently destroyed and data loss will occur.**

    ```
    # Destroy the entire environment
    terraform destroy

    # Rebuild the Omnibus instance
    terraform destroy -target=module.gitlab_omnibus_sandbox.module.gitlab_omnibus_instance
    terraform apply -target=module.gitlab_omnibus_sandbox.module.gitlab_omnibus_instance

    # Rebuild the Kubernetes cluster
    terraform destroy -target=module.gitlab_omnibus_sandbox.module.gitlab_ci_cluster
    terraform apply -target=module.gitlab_omnibus_sandbox.module.gitlab_ci_cluster

    # Rebuild the DNS record
    terraform destroy -target=module.gitlab_omnibus_sandbox.google_dns_record_set.gitlab_omnibus_subdomain_dns_record
    terraform apply -target=module.gitlab_omnibus_sandbox.google_dns_record_set.gitlab_omnibus_subdomain_dns_record
    ```

## Variables, Outputs, and Additional Customization

See the [README.md](README.md) for more information on [variables](README.md#variables) and [outputs](README.md#outputs). You can review the [main.tf](main.tf) file to see all of the resources that are being created with this module, and the respective variables that can be configured to customize them.
